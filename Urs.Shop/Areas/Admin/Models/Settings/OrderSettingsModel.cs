﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Urs.Admin.Validators.Settings;
using Urs.Framework;
using Urs.Framework.Mvc;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Settings
{
    public partial class OrderSettingsModel : BaseModel, ISettingsModel
    {
        [UrsDisplayName("Admin.Configuration.Settings.Order.IsReOrderAllowed")]
        public bool IsReOrderAllowed { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Order.MinOrderSubtotalAmount")]
        public decimal MinOrderSubtotalAmount { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Order.MinOrderTotalAmount")]
        public decimal MinOrderTotalAmount { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Order.AnonymousCheckoutAllowed")]
        public bool AnonymousCheckoutAllowed { get; set; }
        [UrsDisplayName("Admin.Configuration.Settings.Order.TimeOffUnpaidOrderMinutes")]
        public int TimeOffUnpaidOrderMinutes { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Order.AfterSalesEnabled")]
        public bool AfterSalesEnabled { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Order.AfterSalesReasons")]
        public string AfterSalesReasonsParsed { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Order.AfterSalesActions")]
        public string AfterSalesActionsParsed { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Order.NumberOfDaysAfterSalesAvailable")]
        public int NumberOfDaysAfterSalesAvailable { get; set; }

        public string PrimaryStoreCurrencyCode { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Order.OrderIdent")]
        public int? OrderIdent { get; set; }
    }
}
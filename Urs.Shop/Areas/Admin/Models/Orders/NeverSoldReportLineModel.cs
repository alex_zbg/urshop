﻿using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Orders
{
    public partial class NeverSoldReportLineModel : BaseModel
    {
        public int GoodsId { get; set; }
        [UrsDisplayName("Admin.SalesReport.NeverSold.Fields.Name")]
        public string GoodsName { get; set; }
    }
}
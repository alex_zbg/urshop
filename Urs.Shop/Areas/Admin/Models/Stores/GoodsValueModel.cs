﻿using System.Collections.Generic;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    public partial class GoodsValueModel : BaseEntityModel
    {
        public GoodsValueModel()
        {
            Values = new List<AttrValue>();
        }
        /// <summary>
        /// 规格名称
        /// </summary>
        public string Name { get; set; }

        public IList<AttrValue> Values { get; set; }

        public partial class AttrValue : BaseEntityModel
        {
            public string Name { get; set; }
        }
    }
}
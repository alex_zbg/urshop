﻿using System;
using System.Collections.Generic;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Home
{
    public partial class UrShopWikiModel : BaseModel
    {
        public UrShopWikiModel()
        {
            Items = new List<WikiDetailsModel>();
        }

        public List<WikiDetailsModel> Items { get; set; }
        public bool HasNewItems { get; set; }
        public bool HideAdvertisements { get; set; }

        public class WikiDetailsModel : BaseModel
        {
            public string Title { get; set; }
            public string Url { get; set; }
            public string Summary { get; set; }
            public DateTimeOffset PublishDate { get; set; }
        }
    }
}
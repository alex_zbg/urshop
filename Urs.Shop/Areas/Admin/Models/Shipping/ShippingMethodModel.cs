﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Urs.Admin.Validators.Shipping;
using Urs.Framework;
using Urs.Framework.Localization;
using Urs.Framework.Mvc;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Shipping
{
    [Validator(typeof(ShippingMethodValidator))]
    public partial class ShippingMethodModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Configuration.Shipping.Methods.Fields.Name")]
        
        public string Name { get; set; }

        [UrsDisplayName("Admin.Configuration.Shipping.Methods.Fields.Description")]
        
        public string Description { get; set; }

        [UrsDisplayName("Admin.Configuration.Shipping.Methods.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
}
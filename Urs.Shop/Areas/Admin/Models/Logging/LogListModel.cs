﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Logging
{
    public partial class LogListModel : BaseModel
    {
        public LogListModel()
        {
            AvailableLogLevels = new List<SelectListItem>();
        }

        [UrsDisplayName("Admin.System.Log.List.CreateTimeFrom")]
        [UIHint("DateNullable")]
        public DateTime? CreateTimeFrom { get; set; }

        [UrsDisplayName("Admin.System.Log.List.CreateTimeTo")]
        [UIHint("DateNullable")]
        public DateTime? CreateTimeTo { get; set; }

        [UrsDisplayName("Admin.System.Log.List.Message")]
        
        public string Message { get; set; }

        [UrsDisplayName("Admin.System.Log.List.LogLevel")]
        public int LogLevelId { get; set; }


        public IList<SelectListItem> AvailableLogLevels { get; set; }
    }
}
﻿namespace Urs.Admin.Models.Common
{
    public class CheckboxModel
    {
        public int Id { get; set; }

        public bool Checked { get; set; }
    }
}

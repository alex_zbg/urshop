﻿using System;
using Urs.Framework;

namespace Urs.Admin.Models.Common
{
    /// <summary>
    /// Represents a form data model
    /// </summary>
    public partial class FavoritesModel 
    {
        [UrsDisplayName("Admin.Id")]
        public int Id { get; set; }
        [UrsDisplayName("Admin.ContentManagement.Favorites.Fields.Name")]
        public string Name { get; set; }
        [UrsDisplayName("Admin.ContentManagement.Favorites.Fields.Phone")]
        public string GoodsName { get; set; }

        [UrsDisplayName("Admin.ContentManagement.Favorites.Fields.CreatedTime")]
        public DateTime CreateTime { get; set; }
    }
}
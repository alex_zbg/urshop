﻿using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Common
{
    public partial class PluginModel : BaseModel, IPluginModel
    {
        [UrsDisplayName("Admin.Configuration.Plugins.Fields.Group")]

        public string Group { get; set; }

        [UrsDisplayName("Admin.Configuration.Plugins.Fields.FriendlyName")]

        public string FriendlyName { get; set; }

        [UrsDisplayName("Admin.Configuration.Plugins.Fields.SystemName")]

        public string SystemName { get; set; }

        [UrsDisplayName("Admin.Configuration.Plugins.Fields.Version")]

        public string Version { get; set; }

        [UrsDisplayName("Admin.Configuration.Plugins.Fields.Author")]

        public string Author { get; set; }

        [UrsDisplayName("Admin.Configuration.Plugins.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [UrsDisplayName("Admin.Configuration.Plugins.Fields.Configure")]
        public string ConfigurationUrl { get; set; }

        [UrsDisplayName("Admin.Configuration.Plugins.Fields.Installed")]
        public bool Installed { get; set; }

        public bool CanChangeEnabled { get; set; }
        [UrsDisplayName("Admin.Configuration.Plugins.Fields.IsEnabled")]
        public bool IsEnabled { get; set; }

        [UrsDisplayName("Admin.Configuration.Plugins.Fields.Logo")]
        public string LogoUrl { get; set; }
        public bool IsActive { get; set; }
    }
}
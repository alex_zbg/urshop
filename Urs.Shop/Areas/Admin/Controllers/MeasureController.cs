﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Urs.Admin.Models.Directory;
using Urs.Data.Domain.Directory;
using Urs.Data.Domain.Configuration;
using Urs.Services.Configuration;
using Urs.Services.Directory;
using Urs.Services.Localization;
using Urs.Services.Security;
using Urs.Framework;
using Urs.Framework.Controllers;
using Urs.Framework.Extensions;
using Urs.Framework.Kendoui;
using Urs.Framework.Mvc;

namespace Urs.Admin.Controllers
{
	[AdminAuthorize]
    public partial class MeasureController : BaseAdminController
	{
		#region Fields

        private readonly IMeasureService _measureService;
        private readonly MeasureSettings _measureSettings;
        private readonly ISettingService _settingService;
        private readonly IPermissionService _permissionService;
        private readonly ILocalizationService _localizationService;

		#endregion

		#region Constructors

        public MeasureController(IMeasureService measureService,
            MeasureSettings measureSettings, ISettingService settingService,
            IPermissionService permissionService, ILocalizationService localizationService)
		{
            this._measureService = measureService;
            this._measureSettings = measureSettings;
            this._settingService = settingService;
            this._permissionService = permissionService;
            this._localizationService = localizationService;
		}

		#endregion 

		#region Methods

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return HttpUnauthorized();

            return View();
        }

        #region Weights

        [HttpPost]
        public IActionResult Weights(PageRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return HttpUnauthorized();

            var weightsModel = _measureService.GetAllMeasureWeights()
                .Select(x => x.ToModel<MeasureWeightModel>())
                .ToList();
            foreach (var wm in weightsModel)
                wm.IsPrimaryWeight = wm.Id == _measureSettings.BaseWeightId;
            var result = new ResponseResult
            {
                data = weightsModel,
                count = weightsModel.Count
            };

            return Json(result);
        }

        [HttpPost]
        public IActionResult WeightUpdate(MeasureWeightModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMeasures))
                return HttpUnauthorized();
            
            if (!ModelState.IsValid)
            {
                return Json(new ResponseResult { msg = ModelState.SerializeErrors() });
            }

            var weight = _measureService.GetMeasureWeightById(model.Id);
            weight = model.ToEntity(weight);
            _measureService.UpdateMeasureWeight(weight);

            return new NullJsonResult();
        }

        [HttpPost]
        public IActionResult WeightAdd(MeasureWeightModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMeasures))
                return HttpUnauthorized();

            if (!ModelState.IsValid)
            {
                return Json(new ResponseResult { msg = ModelState.SerializeErrors() });
            }

            var weight = new MeasureWeight();
            weight = model.ToEntity(weight);
            _measureService.InsertMeasureWeight(weight);

            return new NullJsonResult();
        }

        [HttpPost]
        public IActionResult WeightDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMeasures))
                return HttpUnauthorized();

            var weight = _measureService.GetMeasureWeightById(id);
            if (weight == null)
                throw new ArgumentException("No weight found with the specified id");

            if (weight.Id == _measureSettings.BaseWeightId)
                return Json(new ResponseResult { msg = _localizationService.GetResource("Admin.Configuration.Shipping.Measures.Weights.CantDeletePrimary") });

            _measureService.DeleteMeasureWeight(weight);

            return new NullJsonResult();
        }

        [HttpPost]
        public IActionResult MarkAsPrimaryWeight(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return HttpUnauthorized();

            var primaryWeight = _measureService.GetMeasureWeightById(id);
            if (primaryWeight != null)
            {
                _measureSettings.BaseWeightId = primaryWeight.Id;
                _settingService.SaveSetting(_measureSettings);
            }

            return Json(new { result = true });
        }

        #endregion

        #region Dimensions

        [HttpPost]
        public IActionResult Dimensions(PageRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMeasures))
                return HttpUnauthorized();

            var dimensionsModel = _measureService.GetAllMeasureDimensions()
                .Select(x => x.ToModel<MeasureDimensionModel>()).ToList();
            foreach (var wm in dimensionsModel)
                wm.IsPrimaryDimension = wm.Id == _measureSettings.BaseDimensionId;
            var model = new ResponseResult
            {
                data = dimensionsModel,
                count = dimensionsModel.Count
            };

            return Json(model);
        }

        [HttpPost]
        public IActionResult DimensionUpdate(MeasureDimensionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMeasures))
                return HttpUnauthorized();

            if (!ModelState.IsValid)
            {
                return Json(new ResponseResult { msg = ModelState.SerializeErrors() });
            }

            var dimension = _measureService.GetMeasureDimensionById(model.Id);
            dimension = model.ToEntity(dimension);
            _measureService.UpdateMeasureDimension(dimension);
            
            return new NullJsonResult();
        }


        [HttpPost]
        public IActionResult DimensionAdd(MeasureDimensionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMeasures))
                return HttpUnauthorized();

            if (!ModelState.IsValid)
            {
                return Json(new ResponseResult { msg = ModelState.SerializeErrors() });
            }

            var dimension = new MeasureDimension();
            dimension = model.ToEntity(dimension);
            _measureService.InsertMeasureDimension(dimension);

            return new NullJsonResult();
        }

        public IActionResult DimensionDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMeasures))
                return HttpUnauthorized();

            var dimension = _measureService.GetMeasureDimensionById(id);
            if (dimension == null)
                throw new ArgumentException("No dimension found with the specified id");

            if (dimension.Id == _measureSettings.BaseDimensionId)
                return Json(new ResponseResult { msg = _localizationService.GetResource("Admin.Configuration.Shipping.Measures.Dimensions.CantDeletePrimary") });

            _measureService.DeleteMeasureDimension(dimension);

            return new NullJsonResult();
        }

        [HttpPost]
        public IActionResult MarkAsPrimaryDimension(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return HttpUnauthorized();

            var primaryDimension = _measureService.GetMeasureDimensionById(id);
            if (primaryDimension != null)
            {
                _measureSettings.BaseDimensionId = id;
                _settingService.SaveSetting(_measureSettings);
            }

            return Json(new { result = true });
        }
        #endregion

        #endregion
    }
}

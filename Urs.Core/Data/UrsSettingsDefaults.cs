﻿
namespace Urs.Core.Data
{
    /// <summary>
    /// Represents default values related to data settings
    /// </summary>
    public static partial class UrsSettingsDefaults
    {
        public static string PluginsFilePath => "~/App_Data/installedPlugins.json";
        /// <summary>
        /// Gets a path to the file that contains data settings
        /// </summary>
        public static string FilePath => "~/App_Data/dataSettings.json";
    }
}
/* 
 * 订单处理
 * Urshop V1.0 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Urs.Core;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Common;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Logging;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Payments;
using Urs.Data.Domain.Shipping;
using Urs.Data.Domain.Configuration;
using Urs.Services.Stores;
using Urs.Services.Common;
using Urs.Services.Coupons;
using Urs.Services.Users;
using Urs.Services.Localization;
using Urs.Services.Logging;
using Urs.Services.Payments;
using Urs.Services.Security;
using Urs.Services.Shipping;
using Urs.Data.Domain.Agents;
using Urs.Services.Agents;

namespace Urs.Services.Orders
{
    public partial class OrderProcessingService : IOrderProcessingService
    {
        #region Fields

        private readonly IOrderService _orderService;
        private readonly IWebHelper _webHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IGoodsService _goodsService;
        private readonly IPaymentService _paymentService;
        private readonly ILogger _logger;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IGoodsSpecParser _goodsSpecParser;
        private readonly IGoodsSpecFormatter _goodsSpecFormatter;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IShippingService _shippingService;
        private readonly IShipmentService _shipmentService;
        private readonly IUserService _userService;
        private readonly IAddressService _addressService;
        private readonly IEncryptionService _encryptionService;
        private readonly IWorkContext _workContext;
        private readonly IActivityLogService _activityLogService;
        private readonly PaymentSettings _paymentSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly OrderSettings _orderSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly BillingAddressSettings _billingAddressSettings;
        private readonly ICouponService _couponService;
        private readonly AgentSettings _agentSettings;
        private readonly IAgentService _agentService;
        private readonly IAgentProductService _agentProductService;

        #endregion

        #region Ctor 构造器

        public OrderProcessingService(IOrderService orderService,
            IWebHelper webHelper,
            ILocalizationService localizationService,
            IGoodsService goodsService,
            IPaymentService paymentService,
            ILogger logger,
            IOrderTotalCalculationService orderTotalCalculationService,
            IPriceCalculationService priceCalculationService,
            IGoodsSpecParser goodsSpecParser,
            IGoodsSpecFormatter goodsSpecFormatter,
            IShoppingCartService shoppingCartService,
            IShippingService shippingService,
            IShipmentService shipmentService,
            IUserService userService,
            IAddressService addressService,
            IEncryptionService encryptionService,
            IWorkContext workContext,
            IActivityLogService activityLogService,
            PaymentSettings paymentSettings,
            RewardPointsSettings rewardPointsSettings,
            OrderSettings orderSettings,
            ShippingSettings shippingSettings,
            BillingAddressSettings billingAddressSettings,
            ICouponService couponService,
            AgentSettings agentSettings,
            IAgentService agentService,
            IAgentProductService agentProductService)
        {
            this._orderService = orderService;
            this._webHelper = webHelper;
            this._localizationService = localizationService;
            this._goodsService = goodsService;
            this._paymentService = paymentService;
            this._logger = logger;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._priceCalculationService = priceCalculationService;
            this._goodsSpecParser = goodsSpecParser;
            this._goodsSpecFormatter = goodsSpecFormatter;
            this._shoppingCartService = shoppingCartService;
            this._workContext = workContext;
            this._shippingService = shippingService;
            this._shipmentService = shipmentService;
            this._userService = userService;
            this._addressService = addressService;
            this._billingAddressSettings = billingAddressSettings;
            this._encryptionService = encryptionService;
            this._activityLogService = activityLogService;
            this._paymentSettings = paymentSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._orderSettings = orderSettings;
            this._shippingSettings = shippingSettings;
            this._couponService = couponService;
            this._agentSettings = agentSettings;
            this._agentService = agentService;
            this._agentProductService = agentProductService;
        }

        #endregion

        #region Utilities

        protected void AwardRewardPoints(Order order)
        {
            if (!_rewardPointsSettings.Enabled)
                return;

            if (_rewardPointsSettings.PointsForPurchases_Amount <= decimal.Zero)
                return;

            if (order.User == null || order.User.IsGuest())
                return;

            int points = (int)Math.Truncate(order.OrderTotal / _rewardPointsSettings.PointsForPurchases_Amount * _rewardPointsSettings.PointsForPurchases_Points);
            if (points == 0)
                return;

            if (order.RewardPointsWereAdded)
                return;

            order.User.AddRewardPointsHistoryEntry(points, string.Format(_localizationService.GetResource("RewardPoints.Message.EarnedForOrder"), order.Id));
            order.RewardPointsWereAdded = true;
            _orderService.UpdateOrder(order);
        }
        protected void ReduceRewardPoints(Order order)
        {
            if (!_rewardPointsSettings.Enabled)
                return;

            if (_rewardPointsSettings.PointsForPurchases_Amount <= decimal.Zero)
                return;

            if (order.User == null || order.User.IsGuest())
                return;

            int points = (int)Math.Truncate(order.OrderTotal / _rewardPointsSettings.PointsForPurchases_Amount * _rewardPointsSettings.PointsForPurchases_Points);
            if (points == 0)
                return;

            if (!order.RewardPointsWereAdded)
                return;

            order.User.AddRewardPointsHistoryEntry(-points, string.Format(_localizationService.GetResource("RewardPoints.Message.ReducedForOrder"), order.Id));
            _orderService.UpdateOrder(order);
        }
        protected void ExchangeRewardPoints(Order order, User user, int exchangePoints)
        {
            if (!_rewardPointsSettings.Enabled)
                return;
            if (order.ExchangePointsAmount > decimal.Zero)
            {
                user.AddRewardPointsHistoryEntry(-exchangePoints,
                    string.Format(_localizationService.GetResource("RewardPoints.Message.RedeemedForOrder"), order.Id),
                    order,
                    order.ExchangePointsAmount);
                _userService.UpdateUser(user);
            }
        }

        protected decimal AgentRate(int customerId, decimal rate)
        {
            var agentUser = _agentService.GetAgentUserByUserId(customerId);

            if (agentUser == null)
                return rate;

            return agentUser.Rate;
        }
        /// <summary>
        /// 奖励分销代理佣金
        /// </summary>
        /// <param name="order"></param>
        public void AwardAgentBonus(Order order)
        {
            if (!_agentSettings.Enabled || !order.AffiliateId.HasValue) return;
            var agentId = order.AffiliateId.Value;

            var price = 0m;
            if (_agentSettings.Method == AgentBonusMethod.OrderTotal)
                price = order.OrderTotal;
            else if (_agentSettings.Method == AgentBonusMethod.OrderSubtotal)
                price = order.OrderSubtotal;
            else if (_agentSettings.Method == AgentBonusMethod.ProductProfit)
                foreach (var item in order.orderItems)
                {
                    var product = item.Goods;
                    price += product.Price - product.ProductCost;
                }

            if (price < 0) return;

            var fee = AgentRate(order.UserId, _agentSettings.Rate) * price / 100;

            var agentBonus = new AgentBonus()
            {
                OrderId = order.Id,
                UserId = order.UserId,
                Price = price,
                AgentId = agentId,
                Fee = fee,
                Cash = false,
                IsValid = false,
                CreateTime = DateTime.Now
            };

            if (_agentSettings.ParentAgentEnabled)
            {
                var agent = _userService.GetUserById(agentId);
                if (agent != null && agent.AffiliateId.HasValue)
                {
                    var parentAgentId = agent.AffiliateId.Value;
                    var parentFee = AgentRate(parentAgentId, _agentSettings.ParentRate) * price / 100;
                    agentBonus.ParentAgentId = parentAgentId;
                    agentBonus.ParentCash = false;
                    agentBonus.ParentFee = parentFee;
                }
            }
            _agentService.InsertAgentBonus(agentBonus);
        }

        protected void ValidAgentBonus(Order order)
        {
            if (!_agentSettings.Enabled || !order.AffiliateId.HasValue) return;

            var agentBonus = _agentService.GetAgentBonusByOrderId(order.Id);
            if (agentBonus != null)
            {
                agentBonus.IsValid = true;
                _agentService.UpdateAgentBonus(agentBonus);
            }
            else
            {
                var msg = string.Format("订单Id:{0},订单号:{1} 分销信息为空", order.Id, order.Code);
                _logger.Information(msg);
            }
        }
        /// <summary>
        /// 取消分销代理佣金
        /// </summary>
        /// <param name="order"></param>
        protected void DisValidAgentBonus(Order order)
        {
            if (!_agentSettings.Enabled || !order.AffiliateId.HasValue) return;

            var agentBonus = _agentService.GetAgentBonusByOrderId(order.Id);
            agentBonus.IsValid = false;
            _agentService.UpdateAgentBonus(agentBonus);
        }

        protected void SetOrderStatus(Order order,
            OrderStatus os, bool notifyUser)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            OrderStatus prevOrderStatus = order.OrderStatus;
            if (prevOrderStatus == os)
                return;

            order.OrderStatusId = (int)os;
            _orderService.UpdateOrder(order);

            order.OrderNotes.Add(new OrderNote()
            {
                Note = string.Format(_localizationService.GetResource("Order.Message.OrderStatusChanged"), os.GetLocalizedEnum(_localizationService)),
                DisplayToUser = false,
                CreateTime = DateTime.Now
            });
            _orderService.UpdateOrder(order);

            if (_rewardPointsSettings.PointsForPurchases_Awarded == order.OrderStatus)
            {
                AwardRewardPoints(order);
            }
            if (_rewardPointsSettings.PointsForPurchases_Canceled == order.OrderStatus)
            {
                ReduceRewardPoints(order);
            }
            if (_agentSettings.BonusForHandout_Awarded == order.OrderStatus)
            {
                ValidAgentBonus(order);
            }
            if (_agentSettings.BonusForHandout_Canceled == order.OrderStatus)
            {
                DisValidAgentBonus(order);
            }
        }

        protected void CheckOrderStatus(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderStatus == OrderStatus.Pending)
            {
                if (order.PaymentStatus == PaymentStatus.Authorized ||
                    order.PaymentStatus == PaymentStatus.Paid)
                {
                    SetOrderStatus(order, OrderStatus.Processing, false);
                }
            }

            if (order.OrderStatus == OrderStatus.Processing)
            {
                if (order.ShippingStatus == ShippingStatus.PartiallyShipped ||
                    order.ShippingStatus == ShippingStatus.Shipped)
                {
                    SetOrderStatus(order, OrderStatus.Delivering, false);
                }
            }

            if (order.OrderStatus != OrderStatus.Cancelled &&
                order.OrderStatus != OrderStatus.Reviewing)
            {
                if (order.PaymentStatus == PaymentStatus.Paid || order.PaymentStatus == PaymentStatus.Authorized)
                {
                    if (order.ShippingStatus == ShippingStatus.ShippingNotRequired || order.ShippingStatus == ShippingStatus.Delivered)
                    {
                        SetOrderStatus(order, OrderStatus.Complete, false);
                    }
                }
            }

            if (order.PaymentStatus == PaymentStatus.Paid && !order.PaidTime.HasValue)
            {
                order.PaidTime = DateTime.Now;
                _orderService.UpdateOrder(order);
            }
        }

        #endregion

        #region Methods
        public string GetSku(Goods goods, string xml)
        {
            var combination = _goodsSpecParser.FindGoodsSpecCombination(goods, xml);
            if (combination == null)
                return goods.Sku;
            return combination.Sku;
        }
        public virtual PlaceOrderResult PlaceOrder(ProcessPaymentRequest processRequest)
        {
            if (processRequest == null)
                throw new ArgumentNullException("processPaymentRequest");

            if (processRequest.OrderGuid == Guid.Empty)
                processRequest.OrderGuid = Guid.NewGuid();

            var result = new PlaceOrderResult();
            try
            {
                #region Order details (user, addresses, totals)

                var user = _userService.GetUserById(processRequest.UserId);
                if (user == null)
                    throw new ArgumentException("用户不存在");

                if (user.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new UrsException("不允许匿名用户下单");

                IList<ShoppingCartItem> cart = user.ShoppingCartItems.Where(sci => sci.Selected).ToList();

                if (cart.Count == 0)
                    throw new UrsException("购物车为空");

                var warnings = _shoppingCartService.GetShoppingCartWarnings(cart);
                if (warnings.Count > 0)
                {
                    var warningsSb = new StringBuilder();
                    foreach (string warning in warnings)
                    {
                        warningsSb.Append(warning);
                        warningsSb.Append(";");
                    }
                    throw new UrsException(warningsSb.ToString());
                }

                foreach (var sci in cart)
                {
                    var sciWarnings = _shoppingCartService.GetShoppingCartItemWarnings(user, sci.Goods, sci.AttributesXml, sci.Quantity, false);
                    if (sciWarnings.Count > 0)
                    {
                        var warningsSb = new StringBuilder();
                        foreach (string warning in sciWarnings)
                        {
                            warningsSb.Append(warning);
                            warningsSb.Append(";");
                        }
                        throw new UrsException(warningsSb.ToString());
                    }
                }

                bool minOrderSubtotalAmountOk = ValidateMinOrderSubtotalAmount(cart);
                if (!minOrderSubtotalAmountOk)
                {
                    decimal minOrderSubtotalAmount = _orderSettings.MinOrderSubtotalAmount;
                    throw new UrsException(string.Format(_localizationService.GetResource("Checkout.MinOrderSubtotalAmount"), PriceFormatter.FormatPrice(minOrderSubtotalAmount)));
                }
                bool minOrderTotalAmountOk = ValidateMinOrderTotalAmount(cart, user);
                if (!minOrderTotalAmountOk)
                {
                    decimal minOrderTotalAmount = _orderSettings.MinOrderTotalAmount;
                    throw new UrsException(string.Format(_localizationService.GetResource("Checkout.MinOrderTotalAmount"), PriceFormatter.FormatPrice(minOrderTotalAmount)));
                }

                decimal orderSubTotal = _orderTotalCalculationService.GetShoppingCartSubTotal(cart);

                bool shoppingCartRequiresShipping = cart.RequiresShipping();

                Address shippingAddress = null;
                string shippingMethodName = string.Empty, shippingRateComputationMethodSystemName = string.Empty;
                if (shoppingCartRequiresShipping)
                {
                    if (user.ShippingAddress == null && processRequest.AddressId == 0)
                        throw new UrsException("必须提供收货地址");

                    if (processRequest.AddressId > 0)
                        shippingAddress = _addressService.GetAddressById(processRequest.AddressId);
                    if (shippingAddress == null)
                        shippingAddress = (Address)user.ShippingAddress.Clone();
                }

                decimal shippingTotal = _orderTotalCalculationService.GetShoppingCartShippingTotal(cart, user);

                decimal payFee = _paymentService.GetAdditionalHandlingFee(orderSubTotal, processRequest.PaymentMethod);

                decimal orderTotal = _orderTotalCalculationService.GetShoppingCartTotal(cart, user);
                int? couponId = null;
                decimal couponAmount = default(decimal);
                if (processRequest.CouponId > 0)
                {
                    var coupon = _couponService.GetUsableCoupon(user.Id, orderTotal, processRequest.CouponId);
                    if (coupon != null)
                    {
                        orderTotal = orderTotal - coupon.Value > 0 ? orderTotal - coupon.Value : 0;
                        couponAmount = coupon.Value;
                        couponId = coupon.Id;
                    }
                }
                decimal pointsAmount = default(decimal); int points = default(int);
                if (processRequest.RedeemedPoint)
                {
                    pointsAmount = _orderTotalCalculationService.RedeemedPointsAmount(user, orderTotal, out points);
                    orderTotal = orderTotal - pointsAmount;
                }
                processRequest.OrderTotal = orderTotal;
                #endregion

                #region Payment workflow

                bool skipPaymentWorkflow = false;
                if (orderTotal == decimal.Zero)
                    skipPaymentWorkflow = true;

                IPaymentMethod paymentMethod = null;
                if (!skipPaymentWorkflow && !string.IsNullOrEmpty(processRequest.PaymentMethod))
                {
                    paymentMethod = _paymentService.LoadPaymentMethodBySystemName(processRequest.PaymentMethod);
                    if (paymentMethod == null)
                        throw new UrsException("支付方式未安装");

                    if (!paymentMethod.IsPaymentMethodActive(_paymentSettings))
                        throw new UrsException("支付方式未启用");
                }
                else
                    processRequest.PaymentMethod = string.Empty;

                ProcessPaymentResult processPaymentResult = null;
                if (!skipPaymentWorkflow && !string.IsNullOrEmpty(processRequest.PaymentMethod))
                {
                    processPaymentResult = _paymentService.ProcessPayment(processRequest);
                }
                else if (string.IsNullOrEmpty(processRequest.PaymentMethod))
                {
                    processPaymentResult = new ProcessPaymentResult() { NewPaymentStatus = PaymentStatus.Pending };
                }
                else if (orderTotal == decimal.Zero)
                    processPaymentResult = new ProcessPaymentResult() { NewPaymentStatus = PaymentStatus.Paid };

                _logger.Information(string.Format("支付状态:{0},订单金额:{1}", processPaymentResult.NewPaymentStatus, orderTotal));

                if (processPaymentResult == null)
                    throw new UrsException("支付方式无效");

                #endregion

                if (processPaymentResult.Success)
                {
                    {
                        #region Save order details

                        var shippingStatus = ShippingStatus.NotYetShipped;
                        if (!shoppingCartRequiresShipping)
                            shippingStatus = ShippingStatus.ShippingNotRequired;

                        var order = new Order()
                        {
                            OrderGuid = processRequest.OrderGuid,
                            UserId = user.Id,
                            OrderIp = _webHelper.GetCurrentIpAddress(),
                            OrderSubtotal = orderSubTotal,
                            OrderShipping = shippingTotal,
                            PaymentMethodAdditionalFee = payFee,
                            ExchangePointsAmount = pointsAmount,
                            AccountAmount = processRequest.Balance,
                            OrderTotal = orderTotal,
                            CouponId = couponId,
                            RefundedAmount = decimal.Zero,
                            CheckoutAttributeDescription = processRequest.CheckoutAttributeDescription,
                            CheckoutAttributesXml = processRequest.CheckoutAttributesXml,
                            Remark = processRequest.Remark,
                            AffiliateId = user.AffiliateId,
                            OrderStatus = orderTotal == decimal.Zero ? OrderStatus.Processing : OrderStatus.Pending,
                            PaymentMethodSystemName = processRequest.PaymentMethod,
                            PaymentBankName = string.Empty,
                            PaymentStatus = processPaymentResult.NewPaymentStatus,
                            PaidTime = null,
                            ShippingAddress = shippingAddress,
                            ShippingStatus = shippingStatus,
                            ShippingMethod = shippingMethodName,
                            ShippingRateMethodSystemName = shippingRateComputationMethodSystemName,
                            CreateTime = DateTime.Now,
                            ShopId=user.ShopId,
                        };
                        if (shippingAddress != null)
                            order.ShippingAddressId = shippingAddress.Id;
                        _orderService.InsertOrder(order);

                        result.PlacedOrder = order;

                        foreach (var sc in cart)
                        {
                            decimal scUnitPrice = _priceCalculationService.GetUnitPrice(sc);
                            decimal scSubTotal = _priceCalculationService.GetSubTotal(sc);

                            string attributeDescription = _goodsSpecFormatter.FormatAttributes(sc.AttributesXml);

                            var itemWeight = _shippingService.GetShoppingCartItemWeight(sc);

                            var opv = new OrderItem()
                            {
                                OrderItemGuid = Guid.NewGuid(),
                                Order = order,
                                GoodsId = sc.GoodsId,
                                GoodsName = sc.Goods.Name,
                                UnitPrice = scUnitPrice,
                                Price = scSubTotal,
                                AttributeDescription = attributeDescription,
                                AttributesXml = sc.AttributesXml,
                                Quantity = sc.Quantity,
                                ItemWeight = itemWeight,
                                Sku = GetSku(sc.Goods, sc.AttributesXml)
                            };
                            order.orderItems.Add(opv);
                            _orderService.UpdateOrder(order);

                            _goodsService.AdjustInventory(sc.Goods, true, sc.Quantity, sc.AttributesXml);
                        }
                        //积分奖励
                        ExchangeRewardPoints(order, user, points);
                        //优惠券核销
                        UsedUserCoupon(user, order);
                        //三级分销
                        AwardAgentBonus(order);
                        cart.ToList().ForEach(sci => _shoppingCartService.DeleteShoppingCartItem(sci, false));

                        #endregion

                        #region Notifications & notes

                        order.OrderNotes.Add(new OrderNote()
                        {
                            Note = "下订单",
                            DisplayToUser = false,
                            CreateTime = DateTime.Now
                        });
                        _orderService.UpdateOrder(order);

                        CheckOrderStatus(order);

                        _activityLogService.InsertActivity(
                            "PublicStore.PlaceOrder",
                            _localizationService.GetResource("ActivityLog.PublicStore.PlaceOrder"),
                            user,
                            order.Id);

                        #endregion
                    }
                }
                else
                {
                    foreach (var paymentError in processPaymentResult.Errors)
                        result.AddError(string.Format("Payment error: {0}", paymentError));
                }
            }
            catch (Exception exc)
            {
                _logger.Error(exc.Message, exc);
                result.AddError(exc.Message);
            }

            #region Process errors

            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i + 1, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                string logError = string.Format("Error while placing order. {0}", error);
                _logger.Error(logError);
            }

            #endregion

            return result;
        }

        public virtual PlaceOrderResult QuickPlaceOrder(ProcessPaymentRequest processPaymentRequest)
        {
            if (processPaymentRequest == null)
                throw new ArgumentNullException("processPaymentRequest");

            if (processPaymentRequest.OrderGuid == Guid.Empty)
                processPaymentRequest.OrderGuid = Guid.NewGuid();

            var result = new PlaceOrderResult();
            try
            {
                #region Order details (user, addresses, totals)
                var user = _userService.GetUserById(processPaymentRequest.UserId);
                if (user == null)
                    throw new ArgumentException("用户不存在");

                if (user.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new UrsException("不允许匿名用户下单");

                Address address = null;
                if (processPaymentRequest.AddressId > 0)
                    address = _addressService.GetAddressById(processPaymentRequest.AddressId);

                if (processPaymentRequest.Shipping != null && processPaymentRequest.AddressId == 0)
                {
                    var shipping = processPaymentRequest.Shipping;
                    address = user.Addresses.ToList().FindAddress(
                           shipping.Name, shipping.PhoneNumber, string.Empty, string.Empty, string.Empty,
                           shipping.Address, string.Empty, shipping.ProvinceName,
                           shipping.CityName, shipping.AreaName, shipping.Zip);

                    if (address == null)
                    {
                        address = new Address();
                        address.Name = shipping.Name;
                        address.PhoneNumber = shipping.PhoneNumber;
                        address.ProvinceName = shipping.ProvinceName;
                        address.CityName = shipping.CityName;
                        address.AreaName = shipping.AreaName;
                        address.Address1 = shipping.Address;
                        address.CreateTime = DateTime.Now;
                        user.UserAddressMappings.Add(new UserAddressMapping() { Address = address });
                    }
                    user.ShippingAddress = address;
                    _userService.UpdateUser(user);
                }
                decimal orderTotal = decimal.Zero;
                foreach (var item in processPaymentRequest.GoodsValues)
                {
                    var goods = _goodsService.GetGoodsBySku(item.Key);
                    if (goods == null || goods.Deleted)
                        result.AddError(string.Format("商品编号：{0}不存在", item.Key));

                    if (!goods.Published)
                        result.AddError(string.Format("商品：{0} 已下架", goods.Name));

                    if (goods.ManageStockEnabled && goods.StockQuantity < item.Value)
                        result.AddError(string.Format("商品：{0} 库存不足", goods.Name));

                    var price = _priceCalculationService.GetFinalPrice(goods);
                    orderTotal += price * item.Value;
                }


                var pointsAmount = _orderTotalCalculationService.RedeemedPointsAmount(user, orderTotal, out int points);

                processPaymentRequest.OrderTotal = orderTotal - pointsAmount;
                #endregion

                #region Payment workflow

                bool skipPaymentWorkflow = false;
                if (orderTotal == decimal.Zero)
                    skipPaymentWorkflow = true;

                IPaymentMethod paymentMethod = null; ProcessPaymentResult processPaymentResult = null;
                if (!skipPaymentWorkflow && !string.IsNullOrEmpty(processPaymentRequest.PaymentMethod))
                {
                    paymentMethod = _paymentService.LoadPaymentMethodBySystemName(processPaymentRequest.PaymentMethod);
                    if (paymentMethod == null)
                        throw new UrsException("支付方式未安装");

                    if (!paymentMethod.IsPaymentMethodActive(_paymentSettings))
                        throw new UrsException("支付方式未启用");
                }
                else
                    processPaymentRequest.PaymentMethod = string.Empty;

                if (!skipPaymentWorkflow && !string.IsNullOrEmpty(processPaymentRequest.PaymentMethod))
                {
                    processPaymentResult = _paymentService.ProcessPayment(processPaymentRequest);
                }
                else if (string.IsNullOrEmpty(processPaymentRequest.PaymentMethod))
                    processPaymentResult = new ProcessPaymentResult() { NewPaymentStatus = PaymentStatus.Pending };
                else
                    processPaymentResult = new ProcessPaymentResult() { NewPaymentStatus = PaymentStatus.Paid };

                if (processPaymentResult == null)
                    throw new UrsException("支付方式无效");

                #endregion

                if (processPaymentResult.Success)
                {
                    {
                        #region Save order details

                        var shippingStatus = ShippingStatus.NotYetShipped;

                        var order = new Order()
                        {
                            OrderGuid = processPaymentRequest.OrderGuid,
                            UserId = user.Id,
                            OrderIp = _webHelper.GetCurrentIpAddress(),
                            OrderSubtotal = orderTotal,
                            OrderShipping = decimal.Zero,
                            PaymentMethodAdditionalFee = decimal.Zero,
                            ExchangePointsAmount = decimal.Zero,
                            CouponId = null,
                            OrderTotal = orderTotal,
                            RefundedAmount = decimal.Zero,
                            CheckoutAttributeDescription = string.Empty,
                            CheckoutAttributesXml = string.Empty,
                            Remark = processPaymentRequest.Remark,
                            AffiliateId = user.AffiliateId,
                            OrderStatus = orderTotal == 0 ? OrderStatus.Processing : processPaymentResult.NewOrderStatus,
                            PaymentMethodSystemName = processPaymentRequest.PaymentMethod,
                            PaymentBankName = string.Empty,
                            PaymentStatus = processPaymentResult.NewPaymentStatus,
                            PaidTime = null,
                            ShippingAddress = address,
                            ShippingStatus = shippingStatus,
                            ShippingMethod = string.Empty,
                            ShippingRateMethodSystemName = string.Empty,
                            CreateTime = DateTime.Now
                        };
                        _orderService.InsertOrder(order);

                        result.PlacedOrder = order;

                        foreach (var pv in processPaymentRequest.GoodsValues)
                        {
                            var goods = _goodsService.GetGoodsBySku(pv.Key);
                            var price = _priceCalculationService.GetFinalPrice(goods);
                            decimal scUnitPrice = price;
                            decimal scSubTotal = price * pv.Value;
                            var opv = new OrderItem()
                            {
                                OrderItemGuid = Guid.NewGuid(),
                                Order = order,
                                GoodsId = goods.Id,
                                GoodsName = goods.Name,
                                UnitPrice = scUnitPrice,
                                Price = scSubTotal,
                                AttributeDescription = string.Empty,
                                AttributesXml = string.Empty,
                                Quantity = pv.Value,
                                ItemWeight = decimal.Zero,
                            };
                            order.orderItems.Add(opv);
                            _orderService.UpdateOrder(order);

                            _goodsService.AdjustInventory(goods, true, pv.Value, string.Empty);
                        }
                        //积分奖励
                        ExchangeRewardPoints(order, user, points);
                        //优惠券核销
                        UsedUserCoupon(user, order);
                        //三级分销
                        AwardAgentBonus(order);
                        #endregion

                        #region Notifications & notes

                        order.OrderNotes.Add(new OrderNote()
                        {
                            Note = "下订单",
                            DisplayToUser = false,
                            CreateTime = DateTime.Now
                        });
                        _orderService.UpdateOrder(order);

                        CheckOrderStatus(order);

                        _activityLogService.InsertActivity(
                            "PublicStore.PlaceOrder",
                            _localizationService.GetResource("ActivityLog.PublicStore.PlaceOrder"),
                            user,
                            order.Id);

                        #endregion
                    }
                }
                else
                {
                    foreach (var paymentError in processPaymentResult.Errors)
                        result.AddError(string.Format("Payment error: {0}", paymentError));
                }
            }
            catch (Exception exc)
            {
                _logger.Error(exc.Message, exc);
                result.AddError(exc.Message);
            }

            #region Process errors

            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i + 1, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                string logError = string.Format("Error while placing order. {0}", error);
                _logger.Error(logError);
            }

            #endregion

            return result;
        }

        protected virtual void UsedUserCoupon(User user, Order order)
        {
            if (!order.CouponId.HasValue) return;

            var cc = _couponService.GetCouponUser(user.Id);
            if (cc != null && cc.Count > 0)
            {
                var couponCustoemr = cc.FirstOrDefault(q => q.CouponId == order.CouponId.Value);
                couponCustoemr.IsUsed = true;
                couponCustoemr.UsedTime = DateTime.Now;
                _couponService.UpdateCouponUser(couponCustoemr);
            }
        }
        public virtual void DeleteOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            ReduceRewardPoints(order);

            foreach (var opv in order.orderItems)
                _goodsService.AdjustInventory(opv.Goods, false, opv.Quantity, opv.AttributesXml);

            order.OrderNotes.Add(new OrderNote()
            {
                Note = "订单被删除",
                DisplayToUser = false,
                CreateTime = DateTime.Now
            });
            _orderService.UpdateOrder(order);

            _orderService.DeleteOrder(order);
        }

        public virtual void Ship(Shipment shipment, bool notifyUser)
        {
            if (shipment == null)
                return;

            var order = _orderService.GetOrderById(shipment.OrderId);
            if (order == null)
                return;

            if (shipment.ShippedTime.HasValue)
                return;

            shipment.ShippedTime = DateTime.Now;
            _shipmentService.UpdateShipment(shipment);

            if (order.HasItemsToAddToShipment() || order.HasItemsToShip())
                order.ShippingStatusId = (int)ShippingStatus.PartiallyShipped;
            else
                order.ShippingStatusId = (int)ShippingStatus.Shipped;
            _orderService.UpdateOrder(order);

            order.OrderNotes.Add(new OrderNote()
            {
                Note = string.Format("发货# {0} 已发送", shipment.Id),
                DisplayToUser = false,
                CreateTime = DateTime.Now
            });
            _orderService.UpdateOrder(order);

            CheckOrderStatus(order);
        }

        public virtual void Deliver(Shipment shipment, bool notifyUser)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            var order = shipment.Order;
            if (order == null)
                throw new Exception("Order cannot be loaded");

            if (shipment.DeliveryTime.HasValue)
                throw new Exception("This shipment is already delivered");

            shipment.DeliveryTime = DateTime.Now;
            _shipmentService.UpdateShipment(shipment);

            if (!order.HasItemsToAddToShipment() && !order.HasItemsToShip() && !order.HasItemsToDeliver())
                order.ShippingStatusId = (int)ShippingStatus.Delivered;

            _orderService.UpdateOrder(order);

            order.OrderNotes.Add(new OrderNote()
            {
                Note = string.Format("发货# {0} 已签收", shipment.Id),
                DisplayToUser = false,
                CreateTime = DateTime.Now
            });
            _orderService.UpdateOrder(order);

            CheckOrderStatus(order);
        }

        public virtual void Complete(int orderId, bool notifyUser)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new Exception("Order cannot be loaded");

            SetOrderStatus(order, OrderStatus.Complete, false);
        }

        public virtual bool CanCancelOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderStatus == OrderStatus.Cancelled)
                return false;

            return true;
        }

        public virtual void CancelOrder(Order order, bool notifyUser)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanCancelOrder(order))
                throw new UrsException("Cannot do cancel for order.");

            SetOrderStatus(order, OrderStatus.Cancelled, notifyUser);

            order.OrderNotes.Add(new OrderNote()
            {
                Note = "订单已取消",
                DisplayToUser = false,
                CreateTime = DateTime.Now
            });
            _orderService.UpdateOrder(order);

            foreach (var opv in order.orderItems)
                _goodsService.AdjustInventory(opv.Goods, false, opv.Quantity, opv.AttributesXml);
        }
        public virtual void CancelUnpaidOrder()
        {
            var minutes = _orderSettings.TimeOffUnpaidOrderMinutes;
            var end = DateTime.Now.AddMinutes(-minutes);
            var orders = _orderService.SearchOrders(endTime: end, os: OrderStatus.Pending);
            foreach (var item in orders)
                CancelOrder(item, false);
        }

        public virtual void ConfirmDeliver()
        {
            var day = _shippingSettings.TimeDeliverDay;
            var end = DateTime.Now.AddDays(-day);
            var start = end.AddDays(-_shippingSettings.TimeConfirmOrderRange);
            var shipments = _shipmentService.GetNotDeliverShipments(start, end);

            foreach (var item in shipments)
                Deliver(item, false);
        }

        public virtual bool CanMarkOrderAsAuthorized(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderStatus == OrderStatus.Cancelled)
                return false;

            if (order.PaymentStatus == PaymentStatus.Pending)
                return true;

            return false;
        }

        public virtual void MarkAsAuthorized(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            order.PaymentStatusId = (int)PaymentStatus.Authorized;
            _orderService.UpdateOrder(order);

            order.OrderNotes.Add(new OrderNote()
            {
                Note = _localizationService.GetResource("Order.Message.MarkAsAuthorized"),
                DisplayToUser = false,
                CreateTime = DateTime.Now
            });
            _orderService.UpdateOrder(order);

            CheckOrderStatus(order);
        }

        public virtual bool CanCapture(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderStatus == OrderStatus.Cancelled ||
                order.OrderStatus == OrderStatus.Pending)
                return false;

            if (order.PaymentStatus == PaymentStatus.Authorized &&
                _paymentService.SupportCapture(order.PaymentMethodSystemName))
                return true;

            return false;
        }

        public virtual IList<string> Capture(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanCapture(order))
                throw new UrsException("Cannot do capture for order.");

            var request = new CapturePaymentRequest();
            CapturePaymentResult result = null;
            try
            {
                request.Order = order;
                result = _paymentService.Capture(request);

                if (result.Success)
                {
                    var paidDate = order.PaidTime;
                    if (result.NewPaymentStatus == PaymentStatus.Paid)
                        paidDate = DateTime.Now;

                    order.PaymentStatus = result.NewPaymentStatus;
                    order.PaidTime = paidDate;
                    _orderService.UpdateOrder(order);

                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = "Order has been captured",
                        DisplayToUser = false,
                        CreateTime = DateTime.Now
                    });
                    _orderService.UpdateOrder(order);

                    CheckOrderStatus(order);

                }
            }
            catch (Exception exc)
            {
                if (result == null)
                    result = new CapturePaymentResult();
                result.AddError(string.Format("Error: {0}. Full exception: {1}", exc.Message, exc.ToString()));
            }


            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                order.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("Unable to capture order. {0}", error),
                    DisplayToUser = false,
                    CreateTime = DateTime.Now
                });
                _orderService.UpdateOrder(order);

                string logError = string.Format("Error capturing order #{0}. Error: {1}", order.Id, error);
                _logger.InsertLog(LogLevel.Error, logError, logError);
            }
            return result.Errors;
        }

        public virtual bool CanMarkOrderAsPaid(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderStatus == OrderStatus.Cancelled)
                return false;

            if (order.PaymentStatus == PaymentStatus.Paid ||
                order.PaymentStatus == PaymentStatus.Refunded ||
                order.PaymentStatus == PaymentStatus.Voided)
                return false;

            return true;
        }

        public virtual void MarkOrderAsPaid(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanMarkOrderAsPaid(order))
                throw new UrsException("You can't mark this order as paid");

            order.PaymentStatusId = (int)PaymentStatus.Paid;
            order.PaidTime = DateTime.Now;
            _orderService.UpdateOrder(order);

            order.OrderNotes.Add(new OrderNote()
            {
                Note = _localizationService.GetResource("Order.Message.OrderMarkedPaid"),
                DisplayToUser = false,
                CreateTime = DateTime.Now
            });
            _orderService.UpdateOrder(order);

            CheckOrderStatus(order);

        }

        public virtual bool CanRefund(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;


            if (order.PaymentStatus == PaymentStatus.Paid &&
                _paymentService.SupportRefund(order.PaymentMethodSystemName))
                return true;

            return false;
        }

        public virtual IList<string> Refund(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanRefund(order))
                throw new UrsException("Cannot do refund for order.");

            var request = new RefundPaymentRequest();
            RefundPaymentResult result = null;
            try
            {
                request.Order = order;
                request.AmountToRefund = order.OrderTotal;
                request.IsPartialRefund = false;
                result = _paymentService.Refund(request);
                if (result.Success)
                {
                    decimal totalAmountRefunded = order.RefundedAmount + request.AmountToRefund;

                    order.RefundedAmount = totalAmountRefunded;
                    order.PaymentStatus = result.NewPaymentStatus;
                    _orderService.UpdateOrder(order);

                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = string.Format("订单已退款. 金额为：{0}", PriceFormatter.FormatPrice(request.AmountToRefund)),
                        DisplayToUser = false,
                        CreateTime = DateTime.Now
                    });
                    _orderService.UpdateOrder(order);

                    CheckOrderStatus(order);
                }

            }
            catch (Exception exc)
            {
                if (result == null)
                    result = new RefundPaymentResult();
                result.AddError(string.Format("Error: {0}. Full exception: {1}", exc.Message, exc.ToString()));
            }

            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                order.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("Unable to refund order. {0}", error),
                    DisplayToUser = false,
                    CreateTime = DateTime.Now
                });
                _orderService.UpdateOrder(order);

                string logError = string.Format("Error refunding order #{0}. Error: {1}", order.Id, error);
                _logger.InsertLog(LogLevel.Error, logError, logError);
            }
            return result.Errors;
        }

        public virtual bool CanRefundOffline(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;

            if (order.PaymentStatus == PaymentStatus.Paid)
                return true;

            return false;
        }

        public virtual void RefundOffline(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanRefundOffline(order))
                throw new UrsException("You can't refund this order");

            decimal amountToRefund = order.OrderTotal;

            decimal totalAmountRefunded = order.RefundedAmount + amountToRefund;

            order.RefundedAmount = totalAmountRefunded;
            order.PaymentStatus = PaymentStatus.Refunded;
            _orderService.UpdateOrder(order);

            order.OrderNotes.Add(new OrderNote()
            {
                Note = string.Format("订单标记为退款，金额为：{0}", PriceFormatter.FormatPrice(amountToRefund)),
                DisplayToUser = false,
                CreateTime = DateTime.Now
            });
            _orderService.UpdateOrder(order);

            CheckOrderStatus(order);
        }

        public virtual bool CanPartiallyRefund(Order order, decimal amountToRefund)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;


            decimal canBeRefunded = order.OrderTotal - order.RefundedAmount;
            if (canBeRefunded <= decimal.Zero)
                return false;

            if (amountToRefund > canBeRefunded)
                return false;

            if ((order.PaymentStatus == PaymentStatus.Paid ||
                order.PaymentStatus == PaymentStatus.PartiallyRefunded) &&
                _paymentService.SupportPartiallyRefund(order.PaymentMethodSystemName))
                return true;

            return false;
        }

        public virtual IList<string> PartiallyRefund(Order order, decimal amountToRefund)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanPartiallyRefund(order, amountToRefund))
                throw new UrsException("Cannot do partial refund for order.");

            var request = new RefundPaymentRequest();
            RefundPaymentResult result = null;
            try
            {
                request.Order = order;
                request.AmountToRefund = amountToRefund;
                request.IsPartialRefund = true;

                result = _paymentService.Refund(request);

                if (result.Success)
                {
                    decimal totalAmountRefunded = order.RefundedAmount + amountToRefund;

                    order.RefundedAmount = totalAmountRefunded;
                    order.PaymentStatus = result.NewPaymentStatus;
                    _orderService.UpdateOrder(order);

                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = string.Format("订单标记为部分退款，金额为：{0}", PriceFormatter.FormatPrice(amountToRefund)),
                        DisplayToUser = false,
                        CreateTime = DateTime.Now
                    });
                    _orderService.UpdateOrder(order);
                    CheckOrderStatus(order);
                }
            }
            catch (Exception exc)
            {
                if (result == null)
                    result = new RefundPaymentResult();
                result.AddError(string.Format("Error: {0}. Full exception: {1}", exc.Message, exc.ToString()));
            }

            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                order.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("无法部分退款.提示： {0}", error),
                    DisplayToUser = false,
                    CreateTime = DateTime.Now
                });
                _orderService.UpdateOrder(order);

                string logError = string.Format("Error refunding order #{0}. Error: {1}", order.Id, error);
                _logger.InsertLog(LogLevel.Error, logError, logError);
            }
            return result.Errors;
        }

        public virtual bool CanPartiallyRefundOffline(Order order, decimal amountToRefund)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;

            decimal canBeRefunded = order.OrderTotal - order.RefundedAmount;
            if (canBeRefunded <= decimal.Zero)
                return false;

            if (amountToRefund > canBeRefunded)
                return false;

            if (order.PaymentStatus == PaymentStatus.Paid ||
                order.PaymentStatus == PaymentStatus.PartiallyRefunded)
                return true;

            return false;
        }

        public virtual void PartiallyRefundOffline(Order order, decimal amountToRefund)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanPartiallyRefundOffline(order, amountToRefund))
                throw new UrsException("You can't partially refund (offline) this order");

            decimal totalAmountRefunded = order.RefundedAmount + amountToRefund;

            order.RefundedAmount = totalAmountRefunded;
            order.PaymentStatus = PaymentStatus.PartiallyRefunded;
            _orderService.UpdateOrder(order);

            order.OrderNotes.Add(new OrderNote()
            {
                Note = string.Format("订单标记为部分线下退款. 退款金额为: {0}", PriceFormatter.FormatPrice(amountToRefund)),
                DisplayToUser = false,
                CreateTime = DateTime.Now
            });
            _orderService.UpdateOrder(order);
            CheckOrderStatus(order);
        }

        public virtual bool CanVoid(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;


            if (order.PaymentStatus == PaymentStatus.Authorized &&
                _paymentService.SupportVoid(order.PaymentMethodSystemName))
                return true;

            return false;
        }

        public virtual IList<string> Void(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanVoid(order))
                throw new UrsException("Cannot do void for order.");

            var request = new VoidPaymentRequest();
            VoidPaymentResult result = null;
            try
            {
                request.Order = order;
                result = _paymentService.Void(request);

                if (result.Success)
                {
                    order.PaymentStatus = result.NewPaymentStatus;
                    _orderService.UpdateOrder(order);

                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = "订单已无效",
                        DisplayToUser = false,
                        CreateTime = DateTime.Now
                    });
                    _orderService.UpdateOrder(order);

                    CheckOrderStatus(order);
                }
            }
            catch (Exception exc)
            {
                if (result == null)
                    result = new VoidPaymentResult();
                result.AddError(string.Format("Error: {0}. Full exception: {1}", exc.Message, exc.ToString()));
            }

            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                order.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("Unable to voiding order. {0}", error),
                    DisplayToUser = false,
                    CreateTime = DateTime.Now
                });
                _orderService.UpdateOrder(order);

                string logError = string.Format("Error voiding order #{0}. Error: {1}", order.Id, error);
                _logger.InsertLog(LogLevel.Error, logError, logError);
            }
            return result.Errors;
        }

        public virtual bool CanVoidOffline(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;


            if (order.PaymentStatus == PaymentStatus.Authorized)
                return true;

            return false;
        }

        public virtual void VoidOffline(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanVoidOffline(order))
                throw new UrsException("You can't void this order");

            order.PaymentStatusId = (int)PaymentStatus.Voided;
            _orderService.UpdateOrder(order);

            order.OrderNotes.Add(new OrderNote()
            {
                Note = "订单标记为无效",
                DisplayToUser = false,
                CreateTime = DateTime.Now
            });
            _orderService.UpdateOrder(order);

            CheckOrderStatus(order);
        }

        public virtual void ReOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            foreach (var opv in order.orderItems)
            {
                _shoppingCartService.AddToCart(opv.Order.User, opv.Goods,opv.AttributesXml,
                    opv.UnitPrice, opv.Quantity, false, out int cartItemId);
            }
        }

        public virtual bool IsAfterSalesAllowed(Order order)
        {
            if (!_orderSettings.AfterSalesEnabled)
                return false;

            if (order == null || order.Deleted)
                return false;

            if (order.OrderStatus != OrderStatus.Complete && order.OrderStatus != OrderStatus.Reviewing)
                return false;

            var rr = _orderService.GetAfterSalesByOrderId(order.Id);

            if (rr != null && rr.Count > 0)
                return false;

            bool numberOfDaysValid = false;
            if (_orderSettings.NumberOfDaysAfterSalesAvailable == 0)
            {
                numberOfDaysValid = true;
            }
            else
            {
                var daysPassed = (DateTime.Now - order.CreateTime).TotalDays;
                numberOfDaysValid = (daysPassed - _orderSettings.NumberOfDaysAfterSalesAvailable) < 0;
            }
            return numberOfDaysValid;
        }

        public virtual bool IsReviewAllowed(Order order)
        {
            if (order == null || order.Deleted)
                return false;

            if (order.OrderStatus != OrderStatus.Reviewing)
                return false;

            return true;
        }
        public virtual bool IsPendingAllowed(Order order)
        {
            if (order == null || order.Deleted)
                return false;
            if (order.OrderStatus == OrderStatus.Pending && order.PaymentMethodSystemName != "Payments.CashOnDelivery"
                && order.CreateTime.AddMinutes(_orderSettings.TimeOffUnpaidOrderMinutes) > DateTime.Now)
                return true;
            return false;
        }

        public virtual bool IsCancelAllowed(Order order)
        {
            if (!_orderSettings.CancelEnabled)
                return false;

            if (order == null || order.Deleted)
                return false;

            if (order.OrderStatus == OrderStatus.Pending)
                return true;

            return false;
        }

        public virtual bool IsDeleteAllowed(Order order)
        {
            if (!_orderSettings.DeleteEnabled)
                return false;

            if (order == null || order.Deleted)
                return false;

            if (order.OrderStatus != OrderStatus.Cancelled)
                return false;

            return true;
        }

        public virtual bool ValidateMinOrderSubtotalAmount(IList<ShoppingCartItem> cart)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            if (cart.Count > 0 && _orderSettings.MinOrderSubtotalAmount > decimal.Zero)
            {
                decimal subTotal = _orderTotalCalculationService.GetShoppingCartSubTotal(cart);

                if (subTotal < _orderSettings.MinOrderSubtotalAmount)
                    return false;
            }

            return true;
        }

        public virtual bool ValidateMinOrderTotalAmount(IList<ShoppingCartItem> cart, User user = null)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            if (cart.Count > 0 && _orderSettings.MinOrderTotalAmount > decimal.Zero)
            {
                decimal orderTotal = _orderTotalCalculationService.GetShoppingCartTotal(cart, user);
                if (orderTotal < _orderSettings.MinOrderTotalAmount)
                    return false;
            }
            return true;
        }

        #endregion
    }
}

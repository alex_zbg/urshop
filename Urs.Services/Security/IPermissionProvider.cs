﻿using System.Collections.Generic;
using Urs.Data.Domain.Security;

namespace Urs.Services.Security
{
    public interface IPermissionProvider
    {
        IEnumerable<PermissionRecord> GetPermissions();
        IEnumerable<DefaultPermissionRecord> GetDefaultPermissions();
    }
}

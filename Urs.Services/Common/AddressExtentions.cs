using System;
using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Common;

namespace Urs.Services.Common
{
    public static class AddressExtentions
    {
        public static Address FindAddress(this List<Address> source,
            string name, string phoneNumber,
            string email, string faxNumber, string company, string address1,
            string address2, string provinceName, string cityName, string areaName,
            string zipPostalCode)
        {
            return source.Find((a) => ((String.IsNullOrEmpty(a.Name) && String.IsNullOrEmpty(name)) || a.Name == name) &&
                ((String.IsNullOrEmpty(a.PhoneNumber) && String.IsNullOrEmpty(phoneNumber)) || a.PhoneNumber == phoneNumber) &&
                ((String.IsNullOrEmpty(a.FaxNumber) && String.IsNullOrEmpty(faxNumber)) || a.FaxNumber == faxNumber) &&
                ((String.IsNullOrEmpty(a.Company) && String.IsNullOrEmpty(company)) || a.Company == company) &&
                ((String.IsNullOrEmpty(a.ProvinceName) && String.IsNullOrEmpty(provinceName)) || a.ProvinceName == provinceName) &&
                ((String.IsNullOrEmpty(a.CityName) && String.IsNullOrEmpty(cityName)) || a.CityName == cityName) &&
                ((String.IsNullOrEmpty(a.AreaName) && String.IsNullOrEmpty(areaName)) || a.AreaName == areaName) &&
                ((String.IsNullOrEmpty(a.Address1) && String.IsNullOrEmpty(address1)) || a.Address1 == address1) &&
                ((String.IsNullOrEmpty(a.Address2) && String.IsNullOrEmpty(address2)) || a.Address2 == address2) &&
                ((String.IsNullOrEmpty(a.ZipPostalCode) && String.IsNullOrEmpty(zipPostalCode)) || a.ZipPostalCode == zipPostalCode));
        }

    }
}

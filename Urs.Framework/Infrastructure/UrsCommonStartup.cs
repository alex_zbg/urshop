﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Urs.Core.Data;
using Urs.Core.Infrastructure;
using Urs.Framework.Infrastructure.Extensions;

namespace Urs.Framework.Infrastructure
{
    public class UrsCommonStartup : IUrsStartup
    {
        /// <summary>
        /// Add and configure any of the middleware
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="configuration">Configuration of the application</param>
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {

            services.AddEntityFrameworkProxies();
            //compression
            services.AddResponseCompression();

            //add options feature
            services.AddOptions();

            //add memory cache
            services.AddMemoryCache();

            //add distributed memory cache
            services.AddDistributedMemoryCache();

            //add HTTP sesion state feature
            services.AddHttpSession();

            //add localization
            services.AddLocalization();

            services.AddUrsAuthentication();
        }

        /// <summary>
        /// Configure the using of added middleware
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public void Configure(IApplicationBuilder application)
        {
            if (DataSettingsManager.DatabaseIsInstalled)
                //use response compression
                application.UseResponseCompression();

            //use static files feature
            Extensions.ApplicationBuilderExtensions.UseStaticFiles(application);

            //use HTTP session
            application.UseSession();

            //use request localization
            application.UseRequestLocalization();

            application.UseUrsAuthentication();
        }

        /// <summary>
        /// Gets order of this startup configuration implementation
        /// </summary>
        public int Order
        {
            //common services should be loaded after error handlers
            get { return 100; }
        }
    }
}
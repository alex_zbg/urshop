using Urs.Core;
namespace Urs.Data.Domain.Stores
{
    public partial class GoodsCategory : BaseEntity
    {
        /// <summary>
        /// ��ƷId
        /// </summary>
        public virtual int GoodsId { get; set; }

        /// <summary>
        /// ����Id
        /// </summary>
        public virtual int CategoryId { get; set; }

        /// <summary>
        /// ����
        /// </summary>
        public virtual int DisplayOrder { get; set; }
    }

}

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Topics;

namespace Urs.Data.Mapping.News
{
    public partial class TopicCategoryMap : UrsEntityTypeConfiguration<TopicCategory>
    {
        public override void Configure(EntityTypeBuilder<TopicCategory> builder)
        {
            builder.ToTable("TopicCategory");
            builder.HasKey(bp => bp.Id);
            builder.Property(bp => bp.Name).IsRequired();
            builder.Property(bp => bp.Description).IsRequired();

            base.Configure(builder);
        }
    }
}

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Shipping;

namespace Urs.Data.Mapping.Shipping
{
    public partial class ShipmentMap : UrsEntityTypeConfiguration<Shipment>
    {
        public override void Configure(EntityTypeBuilder<Shipment> builder)
        {
            builder.ToTable(nameof(Shipment));
            builder.HasKey(s => s.Id);

            builder.Property(s => s.ExpressName);
            builder.Property(s => s.TrackingNumber);
            builder.Property(s => s.TotalWeight).HasColumnType("decimal(18, 4)");

            builder.HasOne(s => s.Order)
                .WithMany(o => o.Shipments)
                .HasForeignKey(s => s.OrderId);

            base.Configure(builder);
        }
    }
}
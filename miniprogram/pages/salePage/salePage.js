import api from '../../api/api'
import {
  returnpost
} from '../../api/conf'
import {
  orderDetail
} from '../../api/conf'
import {
  uploadpicture
} from '../../api/conf'
import {
  returnreasons
} from '../../api/conf'
import {
  returnactions
} from '../../api/conf'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: ['请选择退货原因', '收货地址填错了', '不想买了', '其他原因'],
    operationArr: ['退货退款', '仅退款'],
    index: 0,
    operationIndex: 0,
    proInfo: [],
    orderId: null,
    Images: [],
    imageId: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    wx.showLoading({
      title: '加载中',
    })
    this.setData({
      orderId: options.orderid
    })
    api.get(orderDetail, {
      oid: options.orderid
    }).then(res => {
      that.setData({
        proInfo: res.Data.Items
      })
      wx.hideLoading()
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.getreturnactions()
    this.getreturnreasons()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },
  getreturnactions:function(){
    var that = this
    api.get(returnactions,{

    }).then(res=>{
      that.setData({
        operationArr: res.Data
      })
    })
  },
  getreturnreasons: function () {
    var that = this
    api.get(returnreasons, {

    }).then(res => {
      that.setData({
        array: res.Data
      })
    })
  },
  bindPickerChange: function(e) {
    this.setData({
      index: e.detail.value
    })
  },
  formSubmit: function(e) {
    if (e.detail.value.shuoming == '') {
      wx.showToast({
        title: '请填写退货说明',
        icon: 'none'
      })
      return
    } else if (e.detail.value.phone == '') {
      wx.showToast({
        title: '请填写联系电话',
        icon: 'none'
      })
      return
    }
    var comment = e.detail.value.phone + ',' + e.detail.value.shuoming
    this.returnPost(comment)
  },
  returnPost: function(comment) {
    var that = this
    api.post(returnpost, {
      OrderProductId: that.data.orderId,
      Qty: 1,
      Reason: that.data.array[that.data.index],
      Action: that.data.operationArr[that.data.operationIndex],
      Comments: comment,
      Images: that.data.imageId
    }).then(res => {
      wx.showToast({
        title: '提交成功',
      })
      setTimeout(function() {
        wx.navigateBack({
          delta: 1
        })
      }, 1500)
    })
  },
  chooseImage: function() {
    var that = this
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        wx.showNavigationBarLoading()
        that.setData({
          Images: res.tempFilePaths
        })
        wx.getFileSystemManager().readFile({
          filePath: res.tempFilePaths[0],
          encoding: 'base64',
          success: rep => {
            let imgBase64 = ('data:image/png;base64,' + rep.data)
            api.post(uploadpicture, {
              base64Str: imgBase64
            }).then(res => {
              wx.hideNavigationBarLoading()
              that.setData({
                imageId: res.Data.PictureId
              })
            })
          }
        })
      }
    })
  },
})
﻿using Microsoft.AspNetCore.Mvc;
using Urs.Core;
using Urs.Data.Domain.Shipping;
using Urs.Data.Domain.Configuration;
using Urs.Services.Media;
using Urs.Services.Plugins;
using Urs.Framework.Controllers;

namespace Plugin.Api.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/shipping")]
    [ApiController]
    public partial class ShippingController : BaseApiController
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly IPictureService _pictureService;
        private readonly IWebHelper _webHelper;
        private readonly IPluginFinder _pluginFinder;
        private readonly ShippingSettings _shippingSettings;

        #endregion
        /// <summary>
        /// 构造器
        /// </summary>
        #region Constructors

        public ShippingController(
            IWorkContext workContext, IPictureService pictureService,
            IWebHelper webHelper, IPluginFinder pluginFinder,
            ShippingSettings shippingSettings)
        {
            this._pluginFinder = pluginFinder;
            this._workContext = workContext;
            this._pictureService = pictureService;
            this._webHelper = webHelper;
            this._shippingSettings = shippingSettings;
        }

        #endregion

        #region Utilities

        #endregion

    }
}

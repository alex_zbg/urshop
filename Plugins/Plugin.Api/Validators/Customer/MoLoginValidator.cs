﻿using FluentValidation;
using Plugin.Api.Models.Account;
using Urs.Framework.Validators;

namespace Plugin.Api.Validators.User
{
    public class MoLoginValidator : BaseUrsValidator<MoLogin>
    {

        public MoLoginValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("账号不能为空");
            RuleFor(x => x.Password).NotEmpty().WithMessage("密码不能为空");
        }
    }
}
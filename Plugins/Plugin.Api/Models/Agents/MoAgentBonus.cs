﻿using Plugin.Api.Models.Common;
using System;
using System.Collections.Generic;

namespace Plugin.Api.Models.Agents
{
    /// <summary>
    /// 用户订单
    /// </summary>
    public partial class MoAgentBonus
    {
        public MoAgentBonus()
        {
            Items = new List<MoBonusItem>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// 分红明细
        /// </summary>
        public IList<MoBonusItem> Items { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }

        #region Nested classes
        /// <summary>
        /// 分红明细
        /// </summary>
        public partial class MoBonusItem
        {
            /// <summary>
            /// 订单Id
            /// </summary>
            public int OrderId { get; set; }
            /// <summary>
            /// 订单编号
            /// </summary>
            public string OrderCode { get; set; }
            /// <summary>
            /// 订单金额
            /// </summary>
            public string OrderTotal { get; set; }
            /// <summary>
            /// 用户Id
            /// </summary>
            public int UserId { get; set; }
            /// <summary>
            /// 头像
            /// </summary>
            public string AvatarUrl { get; set; }
            /// <summary>
            /// 昵称
            /// </summary>
            public string Nickname { get; set; }
            /// <summary>
            /// 订单金额
            /// </summary>
            public string Fee { get; set; }
            /// <summary>
            /// 是否已提现
            /// </summary>
            public bool Cash { get; set; }
            /// <summary>
            /// 备注
            /// </summary>
            public string Remark { get; set; }
            /// <summary>
            /// 订单创建时间
            /// </summary>
            public DateTime CreatedTime { get; set; }
        }
        #endregion
    }
}
﻿using Urs.Framework.Mvc;
using Plugin.Api.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Plugin.Api.Models.Checkout
{
    /// <summary>
    /// 购物车
    /// </summary>
    public class MoShoppingCart
    {
        public MoShoppingCart()
        {
            Items = new List<MoCartItem>();
        }
        /// <summary>
        /// 购物车子项
        /// </summary>
        public IList<MoCartItem> Items { get; set; }

        #region Nested Classes

        #endregion
    }

    public partial class MoCartItem
    {
        public MoCartItem()
        {
            Picture = new MoPicture();
            Warnings = new List<string>();
        }
        /// <summary>
        /// 购物车子项Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public MoPicture Picture { get; set; }
        /// <summary>
        /// 商品Id
        /// </summary>
        public int GoodsId { get; set; }
        /// <summary>
        /// Sku
        /// </summary>
        public string GoodsSku { get; set; }
        /// <summary>
        /// 商品名称
        /// </summary>
        public string GoodsName { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public string UnitPrice { get; set; }
        /// <summary>
        /// 小计
        /// </summary>
        public string SubTotal { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 属性
        /// </summary>
        public string AttributeInfo { get; set; }
        /// <summary>
        /// 警告
        /// </summary>
        public IList<string> Warnings { get; set; }

    }
}
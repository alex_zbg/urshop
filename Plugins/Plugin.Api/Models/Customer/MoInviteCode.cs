﻿namespace Plugin.Api.Models.User
{
    /// <summary>
    /// 用户的邀请码
    /// </summary>
    public partial class MoInviteCode
    {
        /// <summary>
        /// 用户Guid
        /// </summary>
        public string Guid { get; set; }
        /// <summary>
        /// 邀请码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 邀请链接
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 邀请二维码链接
        /// </summary>
        public string QrUrl { get; set; }
    }
}
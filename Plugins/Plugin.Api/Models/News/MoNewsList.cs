﻿using System.Collections.Generic;
using Plugin.Api.Models.Common;
using Urs.Framework.Mvc;

namespace Plugin.Api.Models.News
{
    /// <summary>
    /// 文章列表
    /// </summary>
    public partial class MoNewsList 
    {
        public MoNewsList()
        {
            Items = new List<MoNews>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// 文章子项
        /// </summary>
        public IList<MoNews> Items { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }
    }
}
﻿using System.Collections.Generic;
using Plugin.Api.Models.Common;
using Urs.Framework.Mvc;

namespace Plugin.Api.Models.GoodsReview
{
    /// <summary>
    /// 商品评论
    /// </summary>
    public partial class MoGoodsReviewList
    {
        public MoGoodsReviewList()
        {
            GoodsReviews = new List<MoGoodsReview>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// 商品Id
        /// </summary>
        public virtual int GoodsId { get; set; }
        /// <summary>
        /// 商品评论
        /// </summary>
        public IList<MoGoodsReview> GoodsReviews { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }

    }

}